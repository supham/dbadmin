<?php

spl_autoload_register(function ($class) {
	$name = preg_replace_callback('/[a-z][A-Z]/', fn ($m) => $m[0][0] . '-' . strtolower($m[0][1]), $class);
	$name = str_replace('adminer-', '', strtolower($name));

	foreach ([
		__DIR__ .'/../adminer/plugins',
		__DIR__ . '/plugins',
	] as $path) {
		$file = "$path/$name.php";
		if (is_file($file)) {
			include $file;
		}
	}
});


function adminer_object()
{
	// Specify enabled plugins here.
	$plugins = [
		new HighlightSelected(),
		new SaveMenuPos(),
	];

	if (isset($_GET['sqlite'])) {
		// user, passwordnya: "root", "root"
		$plugins[] = new LoginPasswordLess("root");
	}
	// AdminerTheme has to be the last one!
	$plugins[] = new Theme();

	return new AdminerPlugin($plugins);
}

// Include original Adminer or Adminer Editor.
include __DIR__ . "/adminer.php";
